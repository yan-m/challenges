# OS

## Super quiz!

### Instruction
Vous devez répondre au questionnaire directement dans ce README en dessous de chaque question. Ces questions touchent plusieurs aspects de la programmation de bas niveau et les réponses sont généralement assez courtes.

###### Question 1
Quelle est la différence entre le heap et le stack (le monceau et la pile).

Stack is for static memory ex: int x; in a function would be put on the stack.

The heap is for dynamic ex: malloc(3) returns a 3 byte pointer of memory on the heap.

###### Question 2
Que fais la fonction malloc?

It is a wrapper for the operating system function that requests a free page of memory.

###### Question 3
Décrivez le fonctionnement de ce code.

```
uint16_t lastKeys = 0;
uint16_t diff, currentKeys;

while (1) {
	currentKeys = scanKeyboard();
	if ((diff = (uint16_t) (currentKeys & (~lastKeys))) || !currentKeys)
		lastKeys = currentKeys;
	faire_quelque_chose(diff);
}
```

It checks out which keys are newly pressed differently since lastKeys was set and puts them in diff and then sets lastKeys = currentKeys if currentKeys is non zero or if diff is set to a non zero value. Then it does something with the diff value. This is in an infinite loop.


###### Question 4
Que signifie les 8 paramètres de cette commande GCC:  
`gcc -Os -Wall -g3 -flto -lpthread -o main main.c`

-Os = optimize for small size
-Wall = enable all warnings
-g3 = debug symbols
-flto = full link time optimizations
-lpthread = link pthread library
-o output binary
main = output binary name (argument of -o)
main.c = source file

###### Question 5
À quoi sert le DMA dans un microcontrolleur ou un ordinateur.

It's what lets devices like the pci-e gpu or the audio dac access memory directly. To send stuff to those devices you will write to a section of memory that the device has mapped.

###### Question 6
Quelle est la différence entre mutex et sémaphore?

Mutex can only have one thread pass through, semaphore has a counter and can let many through.

###### Question 7
Un microcontrolleur lit, avec son convertisseur analogique à numérique à 12 bits, la température d'une cuve de fermentation à l'aide d'un thermomètre. Votre microcontrolleur fonctionne en 5V. Le thermomètre retourne une valeur entre 0 et 5V correspondant aux températures respectivement entre 15-25 degrées Celcius. Assumez que le comportement du thermomètre est linéaire.
Donnez la précision de lecture de votre microcontrolleur. Dans quel type de variable devriez-vous sauvegarder la valeur lue de façon à conserver le maximum de précision (char, int, long, float, double, etc), pourquoi? S'il y a lieu, expliquez aussi à quoi correspondent les valeurs enregistrées. (Max 10 lignes)


###### Question 8
Expliquer une façon de faire le "debounce" d'un bouton.



###### Question 9
Comment pourriez-vous faire pour mesurer la capacitance (en Farrad) d'un condensateur à l'aide d'un Arduino ?

https://www.arduino.cc/en/Tutorial/CapacitanceMeter

###### Question 10
Que fait ce code et à quoi peut-il servir ? Existe-t-il un équivalent dans la librairie standard du C?

`while (*p++ = *q++) ;`

Can also be rewritten as:
while(*(p++) = *(q++));

Used to copy memory.

###### Question 11
Décrivez les principales fonctions d'un microprocesseur.

Execute des operations.
