#ifndef VEHICLELOGGER_H_
#define VEHICLELOGGER_H_

#include <stdio.h>
#include "ReferencedObject.h"
#include "VehicleInterface.h"

struct VehicleLogger {
    struct ReferencedObject *referencedObject;
    struct ReferencedObject *vehicleReferencedObject;

    struct VehicleInterface originalVehicleCommands;
    struct VehicleInterface *actualVehicleCommands;

    FILE *logFile;
};

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands, struct ReferencedObject *vehicleReferencedObject);
void VehicleLogger_delete(struct VehicleLogger *logger);
void startLogging(void);
void stopLogging(void);

#endif

