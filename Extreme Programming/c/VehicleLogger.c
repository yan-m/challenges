#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "VehicleLogger.h"

static struct VehicleLogger *logger;

/*

Insert any static functions you want here

*/

static void l_startEngine(void)
{
    fprintf(logger->logFile, "Engine started\n");
    logger->originalVehicleCommands.startEngine();
}

static void l_stopEngine(void)
{
    fprintf(logger->logFile, "Engine stopped\n");
    logger->originalVehicleCommands.stopEngine();
}

static void l_setSpeed(short speedInKmh)
{
    fprintf(logger->logFile, "Speed set to %i km/h\n", speedInKmh);
    logger->originalVehicleCommands.setSpeed(speedInKmh);
}

static void l_steer(short angleInDegrees)
{
    fprintf(logger->logFile, "Steered of %i°\n", angleInDegrees);
    logger->originalVehicleCommands.steer(angleInDegrees);
}

static void l_setFlashers(_Bool status)
{
    fprintf(logger->logFile, "Flashers turned %s\n", status ? "on" : "off");
    logger->originalVehicleCommands.setFlashers(status);
}

static unsigned int l_getAmountOfFuelInL(void)
{
    unsigned int fuel = logger->originalVehicleCommands.getAmountOfFuelInL();
    fprintf(logger->logFile, "Amount of fuel remaining in tank: %uL\n", fuel);
}

struct VehicleLogger *VehicleLogger_new(struct VehicleInterface *vehicleCommands,
                                        struct ReferencedObject *vehicleReferencedObject)
{
    struct ReferencedObject *newReferencedObject = ReferencedObject_new();
    FILE *newLogFile = fopen("log.txt", "a+");

    setbuf(newLogFile, NULL);
    fprintf(newLogFile, "\n\n------------ NEW EXECUTION ------------\n\n");

    struct VehicleLogger *pointer = malloc(sizeof(struct VehicleLogger));

    ReferencedObject_reference(vehicleReferencedObject);
    pointer->vehicleReferencedObject = vehicleReferencedObject;
    pointer->referencedObject = newReferencedObject;
    pointer->logFile = newLogFile;

    pointer->originalVehicleCommands = *vehicleCommands;
    pointer->actualVehicleCommands = vehicleCommands;

    logger = pointer;

    return pointer;
}

void VehicleLogger_delete(struct VehicleLogger *logger)
{
    stopLogging();

    if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->referencedObject)) {
        fprintf(logger->logFile, "\n\n------------ END OF EXECUTION ------------\n\n");
        fclose(logger->logFile);

        if(ReferencedObject_unreferenceAndReturnWhetherTheCurrentModuleMustBeFreed(logger->vehicleReferencedObject)) {
            free(logger->vehicleReferencedObject);
        }

        free(logger);
    }
}

void startLogging(void)
{
    logger->actualVehicleCommands->startEngine = &l_startEngine;
    logger->actualVehicleCommands->stopEngine = &l_stopEngine;
    logger->actualVehicleCommands->setSpeed = &l_setSpeed;
    logger->actualVehicleCommands->steer = &l_steer;
    logger->actualVehicleCommands->setFlashers = &l_setFlashers;
    logger->actualVehicleCommands->getAmountOfFuelInL = &l_getAmountOfFuelInL;
    /*
        Complete me: once called, calls to the Vehicle methods (e.g. HondaCivic_setSpeed) should log into log.txt
    */
}

void stopLogging(void)
{
    *logger->actualVehicleCommands = logger->originalVehicleCommands;
}
