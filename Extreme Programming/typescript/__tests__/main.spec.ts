import { Delays, fetchMessageByUserId } from '../src/main';

describe('fetchMessageByUserId function', () => {
  const bobId: number = 123;
  const unknownId: number = 111;
  const expectedMessage: string = 'Hi!';

  it('returns the message when given the id of bob', () => {
    expect.assertions(1);
    return fetchMessageByUserId(bobId).then((data) => {
      expect(data).toBe(expectedMessage);
    })
  });

  it('returns an error when given an unknown id', () => {
    expect.assertions(1);
    return fetchMessageByUserId(unknownId).catch((err) => {
      expect(err).toMatch('error');
    })
  });

});
